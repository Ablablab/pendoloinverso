
//invece di random usare random(0.0 , 1.0)

int N_BOXES =162;//5040;//162;// 5376 with new;//10752 with too much;// 162;         /* Number of disjoint boxes of state space. */
float ALPH_init = 1000;//0.5;qlearning              /* learning rate parameter */
float BETA_init = 0.5;//0.1;qlearning               /* magnitude of noise added to choice */
float GAMMA_init = 0.95;            /* discount factor for future reinf */
float ALPHBETAGAMMA[] = new float[6];
float LAMBDAw  =   0.9;         /* Decay rate for w eligibility trace.NEURNLIKE */
float LAMBDAv   = 0.8;         /* Decay rate for v eligibility trace. NEURONLIKE*/

float w[]= new float[N_BOXES];      /* vector of action weights */
float v[]= new float[N_BOXES];      /* vector of critic weights */
float e[]= new float[N_BOXES];      /* vector of action weight eligibilities */
float xbar[]= new float[N_BOXES];      /* vector of critic weight eligibilities */
float p, oldp, rhat, r;
int failed = 0;
int steps =0;
int box  = 0;





float failure_x = 2.4;
float failure_theta = 12* PI/180;
float q_val[][]= new float[N_BOXES][3];      /* state-action values */ //                    SISTEMA

int failures=0;
long time=0;
long timeRecord=0;
int first_time = 1;
int cur_action, prev_action;
int cur_state, prev_state;
float reward=0;

int input_action = 0;

float W_INIT = 0;


float minM(float x,float y){
  return ((x <= y) ? x : y);
}

float maxM(float x,float y){
  return ((x >= y) ? x : y);
}

float max3(float x, float y, float z){
  float temp =  ((x >= y) ? x : y);
  return ((temp >= z) ? temp : z);
}

void update_q(int prev_state_, int prev_action_, float predicted_value_, float reinf_ ){
   float ALPH = ALPHBETAGAMMA[0];
   float BETA = ALPHBETAGAMMA[1];
   float GAMMA = ALPHBETAGAMMA[2];
   q_val[prev_state_][prev_action_+1] += ALPH * (reinf_ + GAMMA * predicted_value_ - q_val[prev_state_][prev_action_+1]);
}

int get_action(float x_,float x_dot_,float theta_,float theta_dot_)        
{
   int i,j;
   float predicted_value;          /* max_{b} Q(t, ss, b) */
   float ALPH = ALPHBETAGAMMA[0];
   float BETA = ALPHBETAGAMMA[1];
   float GAMMA = ALPHBETAGAMMA[2];
   float reinf = 0.0;
   
   
   if(fails(x_,x_dot_,theta_,theta_dot_)==1){
     
     if(first_time == 1){  //fallimento nel caso iniziale. dato che il caso iniziale è gia fuori dallo spazio non posso aggiornare q value 
       first_time = 0;
     }
     
     if (first_time != 1 && cur_action != -2){     //fallimento generico non nel caso iniziale
        if(x_ >= 0){
          cur_action *= -1;
        }
        update_q(cur_state, cur_action, 0, -1.0);
     }
     return -2;
   }
   
   else{                    //caso ammissibile
     prev_state = cur_state;
     prev_action = cur_action;
     cur_state = get_box_old(x_, x_dot_, theta_, theta_dot_);

     if(first_time == 1){  // nel caso iniziale
        reset_controller();   /* set state and action to null values */
        for (i = 0; i < N_BOXES; i++){
           for (j = 0; j < 3; j++){
              q_val[i][j] = W_INIT;
           }
        } 
        if(INIZIALIZZARE == 1){
           for (float ix = -0.9;ix<1.5;ix+=0.3){
             for (float ixdot = -1.0;ixdot<1;ixdot+=0.3){
               for (float itheta = -PI/180*7;itheta<PI/180*7;itheta+=PI/360/3){
                 for (float ithetadot = -0.5;ithetadot<0.5;ithetadot+=0.1){
                   if(get_box_old(ix,ixdot,itheta,ithetadot)!=-1){
                     if(itheta>0.0){
                       q_val[get_box_old(ix,ixdot,itheta,ithetadot)][1] = -valoreIniziale;
                       q_val[get_box_old(ix,ixdot,itheta,ithetadot)][2] = -valoreIniziale;
                     }
                     if(itheta<0.0){
                       q_val[get_box_old(ix,ixdot,itheta,ithetadot)][1] =-valoreIniziale;
                       q_val[get_box_old(ix,ixdot,itheta,ithetadot)][0] =-valoreIniziale;
                     }
                   }
                 }
               }
             }
           }
        }
        first_time = 0;
      }
  
    //
    //  fase di aggiornamento
    //
  
       else if (prev_action != -2){        //generica iterazione ammissibile
          predicted_value = max3(q_val[cur_state][0], q_val[cur_state][1], q_val[cur_state][2]);
          float reinff = -0.1;
          if(abs(theta_) == PI/180 * 0.4)
            reinff = 0;
          update_q(prev_state, prev_action, predicted_value, reinff);
       }
     
     //
     //      Fase di decisione
     //
     
     
       if(manual == 1){    //controllo manuale
         cur_action = input_action;
         if (millis() - last_input > 100 && last_input != 0){
           input_action = 0;
         }
       }
       
       if(manual == 0){
         if (q_val[cur_state][0] + random(-BETA, BETA) <= q_val[cur_state][2]){
           if (q_val[cur_state][2] + random(-BETA, BETA) <= q_val[cur_state][1]){
             cur_action =0;
           }
           else{
             cur_action = 1;
           }
         }
         else{
           if (q_val[cur_state][0] + random(-BETA, BETA) <= q_val[cur_state][1]){
              cur_action = 0;
           }
           else{
             cur_action = -1;
           }
         }
       }
       return cur_action;
   }
}

void reset_controller()
{
   failures ++;
   cur_state = prev_state = 0;
   cur_action = prev_action = -2;   /* "null" action value */
   if(timeRecord < millis()-time){
     timeRecord = time;
   }
   time = millis();
   kart.reset();
}


/*----------------------------------------------------------------------
   get_box:  Given the current state, returns a number from 1 to 162
  designating the region of the state space encompassing the current state.
  Returns a value of -1 if a failure state is encountered.
----------------------------------------------------------------------*/

float one_degree = 0.0174532 ; /* 2pi/360 */
float three_degrees = 0.05233333;
float six_degrees  = 0.1047192;
float nine_degrees = 0.15707963;
float fifteen_degrees = 0.2617993877;
float twelve_degrees = 0.2094384;
float thirtyfive_degrees = 0.6108652;
float fifty_degrees = 0.87266;

int get_box_old(float x_, float x_dot_,float theta_,float theta_dot_)
{
  int box=0;
 
  if (fails(x_,x_dot_,theta_,theta_dot_) == 1)          {return(-1);} /* to signal failure */

  if (x_ < -0.8)             box = 0;
  else if (x_ < 0.8)              box = 1;
  else                         box = 2;

  if (x_dot_ < -0.5)            ;
  else if (x_dot_ < 0.5)                box += 3;
  else                      box += 6;

  if (theta_ < -six_degrees)          ;
  else if (theta_ < -one_degree)        box += 9;
  else if (theta_ < 0)            box += 18;
  else if (theta_ < one_degree)          box += 27;
  else if (theta_ < six_degrees)        box += 36;
  else                   box += 45;

  if (theta_dot_ < -fifty_degrees)   ;
  else if (theta_dot_ < fifty_degrees)  box += 54;
  else                                 box += 108;

  return(box);
}


int fails(float x, float x_dot, float theta, float theta_dot)
{
  if (x < -failure_x || x > failure_x  || theta < -failure_theta || theta > failure_theta){
    println("errore: "+x+", "+ x_dot+", "+ theta*180/PI+", "+ theta_dot);    
    return 1;
  }
  else{
    return 0;  
  }
}