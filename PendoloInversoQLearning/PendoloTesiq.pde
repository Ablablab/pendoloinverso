int pause = 0;
int selector = 0;
int selector_file = 0;
int manual = 0;
long last_input = 0;
int randomStart = 0;
int INIZIALIZZARE = 0;
float valoreIniziale = 0.9; 

float F=0;

int canvas_x = 1000;
int canvas_y = 500;

Cart kart;

color background = color(255,255,255);

void setup() {
  size(1000,500);
  smooth();
  
  scale(1,-1);
  frameRate(50);
  
  ALPHBETAGAMMA[0] = ALPH_init;
  ALPHBETAGAMMA[1] = BETA_init;
  ALPHBETAGAMMA[2] = GAMMA_init;
  kart = new Cart(new PVector(0,0), +0.00);

  ALPHBETAGAMMA[3] = kart.p_cart_start.x;
  ALPHBETAGAMMA[4] = kart.theta_start;
  ALPHBETAGAMMA[5] = kart.m2;

  
  
}

void draw() {
  translate(canvas_x/2,canvas_y*2.8/4);
  scale(1,-1);
  if(pause == 0){
  
      background(background);
      stroke(220);
      line(-1000,0,1000,0);
      
      stroke(220);
      line(0,-1000,0,1000);
      
      //kart.update();qlearning
      //kart.learn();
      
      get_actionNeuron(kart.p_cart.x,kart.p_cart_dot.x,kart.theta,kart.theta_dot);
      //kart.update();
      kart.render();
      parameters();
      
    
  }
  
}

void keyPressed(){
    if (key==' '){
    
  }
   
   else if (key=='p'){
     pause = 1-pause;
   }
   else if (key=='a'){
     selector = 0;
   }
   else if (key=='b'){
     selector = 1;
   }
   else if (key=='g'){
     selector = 2;
   }
   else if (key=='x'){
     selector = 3;
   }
      else if (key=='t'){
     selector = 4;
   }
      else if (key=='m'){
     selector = 5;
   }
   else if(key == 'd'){
     manual = 1-manual;
   }
   else if(key == 'r'){
     randomStart = 1-randomStart;
   }
   
   else if (key=='1'){
     selector_file = 1;
   }
   else if (key=='2'){
     selector_file = 2;
   }
   else if (key=='3'){
     selector_file = 3;
   }
   else if (key=='4'){
     selector_file = 4;
   }
   else if (key=='5'){
     selector_file = 5;
   }
   else if (key=='0'){
     selector_file = 0;
   }
   else if (key=='s'){
    if(selector_file != 0){
      println("saved to data" + selector_file + "("+N_BOXES+").dat" );
      String exp[] = new String[N_BOXES];
      for (int i = 0; i< N_BOXES; i++){
        exp[i] = join(str(q_val[i]), ":");
      }
      
      saveStrings("data"+selector_file+ "("+N_BOXES+").dat",exp);
      
      selector_file = 0;
    }
   }
   else if (key=='l'){
      if(selector_file != 0){
        println("loaded from " + selector_file + ".dat" );
        String lines[] = loadStrings("data" + selector_file + "(" + N_BOXES +").dat");
        String lin[];
        for(int i = 0; i < N_BOXES; i++){
          lin = split(lines[i],":");
          q_val[i] = float(lin);
        }
           
        selector_file = 0;
    }
   }
   
   
   
   
   else if(keyCode == UP){
     if(selector <3) {
       ALPHBETAGAMMA[selector] += (1-ALPHBETAGAMMA[selector])/10;
     }
     else if (selector == 3){
       
      ALPHBETAGAMMA[selector] += 0.10;
     }
     else if (selector == 4){
       ALPHBETAGAMMA[selector] += PI/180;
     }
     else if (selector == 5){
       ALPHBETAGAMMA[selector] += 1;
     }
   }
   
   else if(keyCode == DOWN){
     if(selector <3) {
       ALPHBETAGAMMA[selector] -= (ALPHBETAGAMMA[selector])/10;
     }
     else if (selector == 3){
       ALPHBETAGAMMA[selector] -= 0.10;
     }
     else if (selector == 4){
       ALPHBETAGAMMA[selector] -= PI/180;
     }
     else if (selector == 5){
       ALPHBETAGAMMA[selector] -= 1;
     }
   }
   
   if(manual == 1){
     if(keyCode == RIGHT){
       input_action = 1;
       last_input = millis();
     }
     if(keyCode == LEFT){
       input_action = -1;
       last_input = millis();
     }
   }
}

void parameters(){
  pushMatrix();
  resetMatrix();
  stroke(244,0,0);
  text("#: " + failures,500,10);
  text("ACTUAL TIME: " + (millis()-time)/1000.0,10,10);
  text("RECORD TIME: " + timeRecord/1000.0,10,30);
  text("THETA: " + (int)((kart.theta-PI)/PI*180),10,50);
  text("X: " + (int)(kart.p_cart.x),10,70);
  text("X_dot: " + (int)(kart.p_cart_dot.x),10,90);
  text("theta_dot: " + (int)(kart.theta_dot/PI*180),10,110);
  text("reward x: " + (int)(- abs((float)kart.p_cart.x)/(canvas_x/2)*100),10,130);
  text("reward theta: " + (int)(- abs((kart.theta-PI)/PI)*100),10,150);

  printinput();
  text(frameRate ,canvas_x - 100,10);

  popMatrix();

}

void printinput(){
  fill(0);
  if(selector == 0){
    fill(230,0,0);
  }
  text("ALPHA: " + ALPHBETAGAMMA[0],10,canvas_y - 110);
  fill(0);
  if(selector == 1){
    fill(230,0,0);
  }
  text("BETA: " + ALPHBETAGAMMA[1],10,canvas_y - 90);
  fill(0);
  if(selector == 2){
    fill(230,0,0);
  }
  text("GAMMMA: " + ALPHBETAGAMMA[2],10,canvas_y - 70);
  fill(0);
  if(selector == 3){
    fill(230,0,0);
  }
  text("X start: " + ALPHBETAGAMMA[3],10,canvas_y - 50);
  fill(0);
  if(selector == 4){
    fill(230,0,0);
  }
  text("theta start: " + (int)((ALPHBETAGAMMA[4]-PI)/PI*180),10,canvas_y - 30);
  fill(0);
  if(selector == 5){
    fill(230,0,0);
  }
  text("mass: " + ALPHBETAGAMMA[5],10,canvas_y - 10);
  fill(0);

}