class Cart {

  PVector p_cart; // Location of cart
  PVector p_mass; // Location of mass
  PVector p_joint; // Location of cart joint
  PVector p_cart_dot; // Velocity of cart
  PVector p_cart_dot_2; //acceleration of cart
 
  
  PVector p_cart_start; //start location of cart
  float theta_start; // start value of theta

  float theta = 0; 
  float theta_dot = 0;
  float theta_dot_2 = 0;
  
  float FORCE_MAG= 10.0;
  long lastUpdate = 0;
  
  float meters = 10;
  float g = 9.81;
  float m1 = 1;
  float m2 = 0.1;
  float l = 0.5;
  

  PVector cart_size = new PVector(40,25);
  float wheel_r = 3;
    
  Cart(PVector cart_input, float theta_input){
    theta_start = theta_input;
    theta = theta_start;
    p_cart_start = cart_input.copy();
    p_cart = p_cart_start.copy();
    
    p_cart_dot = new PVector(00,0);
    p_cart_dot_2 = new PVector(0,0);
    
   
  }

  void reset(){
    kart.theta_dot = kart.p_cart_dot.x = 0;
    kart.p_cart_start.x = ALPHBETAGAMMA[3];
    if (abs(kart.p_cart_start.x) >= failure_x){
      kart.p_cart.x = (failure_x * 9 / 10) * kart.p_cart_start.x / abs(kart.p_cart_start.x);
    }
    else{
      kart.p_cart.x = kart.p_cart_start.x;
    }
    kart.theta_start = ALPHBETAGAMMA[4];
    //if(abs(kart.theta_start-PI) >= failure_theta){
      if(abs(kart.theta_start) >= failure_theta){
      kart.theta = (failure_theta * 9 / 10) * kart.theta_start / abs(kart.theta_start);
    }
    else{
      kart.theta = kart.theta_start;
    }
    kart.m2 = abs(ALPHBETAGAMMA[4]);
    if(randomStart == 1){
      kart.p_cart.x = random(-3.5,3.5);
      kart.p_cart_dot.x = random(-0.3,0.3);
      theta_dot = random(-0.2,0.2);
      theta=PI + random(-3,3)/180*PI;
    }
    last_input = 0;
    input_action = 0;
  
  }

  void learn(){
    int action;

   //reward = - abs(((float)kart.p_cart.x)/(canvas_x/2))/20 - abs((kart.theta-PI)/PI);
   //reward = 0;

   action = get_action(kart.p_cart.x, kart.p_cart_dot.x, kart.theta-PI, kart.theta_dot);
    if(action == 1){
      F = FORCE_MAG;
    }          
    else if(action == -1){
      F = -FORCE_MAG;
    }
    else if(action == 0){
      F = 0;
    }
    else if(action == -2){
      
      reset_controller();
    }
  }


void update(){
    
    //theta_dot_2 = (F/l + m2*l*theta_dot*theta_dot*sin(theta) + (m1+m2)/l*g*tan(theta)) / 
    //                (m2*cos(theta) - (m1 +m2)/cos(theta));
    //p_cart_dot_2.x = (-g * sin(theta) - l*theta_dot_2) / cos(theta);
    float costheta = cos(theta);
    float sintheta = sin(theta);

    float temp = (F + l *m2* kart.theta_dot * kart.theta_dot * sintheta)
             / (m1+m2);

    kart.theta_dot_2 = (g * sintheta - costheta* temp)
         / ( l * (4.0/3 - m2 * costheta * costheta
                                              / (m1+m2)));

    p_cart_dot_2.x  = (temp - l *m2* theta_dot_2* costheta /(m1+m2));

    
    
    theta_dot += theta_dot_2 * (millis()-lastUpdate)/1000.0;
    theta+= theta_dot *(millis()-lastUpdate)/1000.0;
    
    p_cart_dot.x += p_cart_dot_2.x *(millis()-lastUpdate)/1000.0;
    p_cart.x += p_cart_dot.x *(millis()-lastUpdate)/1000.0;
    
    lastUpdate = millis();
    
    //if(theta <0){
    //  theta = 2*PI+theta;
    //}
    //if(theta > PI){
    //  theta = theta - 2*PI;
    //}
    
    if(p_cart.x > canvas_x/2 - cart_size.x/2){
      p_cart_dot.x = - p_cart_dot.x/5;
      p_cart.x = canvas_x/2 - cart_size.x/2;
    }
    if(p_cart.x < -canvas_x/2 + cart_size.x/2 ){
      p_cart_dot.x = - p_cart_dot.x/5;
      p_cart.x = -canvas_x/2 + cart_size.x/2;
    }
    
    
  
  }
  
  
  void render(){
    float sizeScale = canvas_x / meters;
    float xpix = sizeScale * kart.p_cart.x;
    float ypix = sizeScale * kart.p_cart.y;
    float lpix = sizeScale * l;
    stroke(50); //pavimento
    line(-1000, -cart_size.y/2 - wheel_r ,1000, -cart_size.y/2 - wheel_r);
    
    translate(xpix, ypix);
    //rettangolo del cart  
    //rect(-cart_size.x/2, - cart_size.y/2, cart_size.x, cart_size.y);
    pushMatrix();
    noStroke();
    fill(200,0,0);
    if(F > 0){
      //translate(cart_size.x/2,0);
      //rotate(PI/2);
      fill(50);
      ellipse(1,0,+cart_size.y + 2*wheel_r,+cart_size.y + 2*wheel_r);
      fill(100);
      ellipse(3,0,+cart_size.y + 2*wheel_r,+cart_size.y + 2*wheel_r);
      fill(200);
      ellipse(5,0,+cart_size.y + 2*wheel_r,+cart_size.y + 2*wheel_r);

    }
    if(F < 0){
      //translate(-cart_size.x/2,0);
      //rotate(-PI/2);
      fill(50);
      ellipse(-1,0,+cart_size.y + 2*wheel_r,+cart_size.y + 2*wheel_r);
      fill(100);
      ellipse(-3,0,+cart_size.y + 2*wheel_r,+cart_size.y + 2*wheel_r);
      fill(200);
      ellipse(-5,0,+cart_size.y + 2*wheel_r,+cart_size.y + 2*wheel_r);

    }
    if(F == 0){
      //translate(-0,-cart_size.y/2);
      //rotate(-PI);
      ellipse(0,0,+cart_size.y + 2*wheel_r,+cart_size.y + 2*wheel_r);
    }
    popMatrix();
    fill(50);
    ellipse(0,0,+cart_size.y + 2*wheel_r,+cart_size.y + 2*wheel_r);

  
    //ruote
    fill(200);
    //ellipse(-cart_size.x/3,  - cart_size.y/2, 2*wheel_r,2*wheel_r);
    //ellipse(+cart_size.x/3,  - cart_size.y/2, 2*wheel_r,2*wheel_r);
        rotate(PI);

    //giunto
    fill(150);
    noStroke();
    ellipse(0,0, cart_size.y/5, cart_size.y/5);
    pushMatrix();
    
    //oriento x come il braccio
    rotate(-theta - PI/2);
    //braccio
    rect(0,-cart_size.y/10, lpix, cart_size.y/5); 
    //mi sposto al centro della massa con gli assi cartesiani
    translate(lpix , 0);
    fill(0); 
    ellipse(0,0, cart_size.y/2, cart_size.y/2); //massa appesa
    noFill();
    popMatrix();
  }

}