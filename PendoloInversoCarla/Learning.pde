
//invece di random usare random(0.0 , 1.0)

int N_BOXES =162;//5040;//162;// 5376 with new;//10752 with too much;// 162;         /* Number of disjoint boxes of state space. */
float ALPH_init = 0.5f;              /* learning rate parameter */
float BETA_init = 0.1f;               /* magnitude of noise added to choice */
float GAMMA_init = 0.999f;            /* discount factor for future reinf */
float ALPHBETAGAMMA[] = new float[6];
float failure_x = 1.0f;
float failure_theta = 13.0f* PI/180;
float q_val[][]= new float[N_BOXES][3];      /* state-action values */ //                    SISTEMA

int failures=0;
long time=0;
long timeRecord=0;
int first_time = 1;
int cur_action, prev_action;
int cur_state, prev_state;
float reward=0;

int input_action = 0;





float W_INIT = 0;


public float minM(float x,float y){
  return ((x <= y) ? x : y);
}

public float maxM(float x,float y){
  return ((x >= y) ? x : y);
}

public float max3(float x, float y, float z){
  float temp =  ((x >= y) ? x : y);
  return ((temp >= z) ? temp : z);
}

public int max3index(float x, float y, float z){
  int temp =  ((x >= y) ? -1 : 0);
  return ((maxM(x,y) >= z) ? temp : 1);
}

public float average3(float x, float y, float z){
  x = 1 + x;
  y = 1 + y;
  z = 1 + z;
  return (x + 2*y + 3*z)/(x+y+z)-2;
}

public float var3(float x, float y, float z, float average){
  x = 1 + x;
  y = 1 + y;
  z = 1 + z;
  float tot = x + y + z;
  x = x / tot;
  y = y / tot;
  z = z / tot;
  average = average +2;
  return  (1-average)*(1-average)*x + (2-average)*(2-average)*y + (3-average)*(3-average)*z ;
}

public void update_q(int prev_state_, int prev_action_, float predicted_value_, float reinf_ ){
   float ALPH = ALPHBETAGAMMA[ALPHA_INDEX];
   float BETA = ALPHBETAGAMMA[BETA_INDEX];
   float GAMMA = ALPHBETAGAMMA[GAMMA_INDEX];
   q_val[prev_state_][prev_action_+1] += ALPH * (reinf_ + GAMMA * predicted_value_ - q_val[prev_state_][prev_action_+1]);
}

public int get_action(float x_,float x_dot_,float theta_,float theta_dot_)        
{
   int i,j;
   float predicted_value;          /* max_{b} Q(t, ss, b) */
   float ALPH = ALPHBETAGAMMA[ALPHA_INDEX];
   float BETA = ALPHBETAGAMMA[BETA_INDEX];
   float GAMMA = ALPHBETAGAMMA[GAMMA_INDEX];
   float reinf = 0.0f;
   
   
   if(fails(x_,x_dot_,theta_,theta_dot_)==1){
     
     if(first_time == 1){  //fallimento nel caso iniziale. dato che il caso iniziale \u00e8 gia fuori dallo spazio non posso aggiornare q value 
       first_time = 0;
     }
     
     if (first_time != 1 && cur_action != -2){     //fallimento generico non nel caso iniziale
        if(x_ >= 0){
          cur_action *= -1;
        }
        update_q(cur_state, cur_action, 0, -1.0f);
     }
     return -2;
   }
   
   else{                    //caso ammissibile
     prev_state = cur_state;
     prev_action = cur_action;
     
     cur_state = get_box_old(x_, x_dot_, theta_, theta_dot_);

    
     if(first_time == 1){  // nel caso iniziale
        reset_controller();   /* set state and action to null values */
        for (i = 0; i < N_BOXES; i++){
           for (j = 0; j < 3; j++){
              q_val[i][j] = W_INIT;
           }
        } 
        if(INIZIALIZZARE == 1){
           for (float ix = -1000/4-100;ix<1500;ix+=300){
             for (float ixdot = -50;ixdot<60;ixdot+=20){
               for (float itheta = -0.03f;itheta<0.03f;itheta+=0.003f){
                 for (float ithetadot = -0.5f;ithetadot<0.5f;ithetadot+=0.1f){
                   if(get_box_old(ix,ixdot,itheta,ithetadot)!=-1){
                     if(itheta>0.0f){
                       q_val[get_box_old(ix,ixdot,itheta,ithetadot)][1] = -valoreIniziale;
                       q_val[get_box_old(ix,ixdot,itheta,ithetadot)][2] = -valoreIniziale;
                     }
                     if(itheta<0.0f){
                       q_val[get_box_old(ix,ixdot,itheta,ithetadot)][1] =-valoreIniziale;
                       q_val[get_box_old(ix,ixdot,itheta,ithetadot)][0] = -valoreIniziale;
                     }
                   }
                 }
               }
             }
           }
        }
        first_time = 0;
      }
  
    //
    //  fase di aggiornamento
    //
  
       else if (prev_action != -2){        //generica iterazione ammissibile
          predicted_value = max3(q_val[cur_state][0], q_val[cur_state][1], q_val[cur_state][2]);
          update_q(prev_state, prev_action, predicted_value, 0);
       }
     
     //
     //      Fase di decisione
     //
     
     
       if(manual == 1){    //controllo manuale
         cur_action = input_action;
         if (millis() - last_input > 100 && last_input != 0){
           input_action = 0;
         }
       }
       
       if(manual == 0){
         if (q_val[cur_state][0] + random(-BETA, BETA) <= q_val[cur_state][2]){
           if (q_val[cur_state][2] + random(-BETA, BETA) <= q_val[cur_state][1]){
             cur_action =0;
           }
           else{
             cur_action = 1;
           }
         }
         else{
           if (q_val[cur_state][0] + random(-BETA, BETA) <= q_val[cur_state][1]){
              cur_action = 0;
           }
           else{
             cur_action = -1;
           }
         }
       }
       return cur_action;
   }
}

public void reset_controller()
{
   cur_state = prev_state = 0;
   cur_action = prev_action = -2;   /* "null" action value */
   if(timeRecord < millis()-time){
     timeRecord = time;
   }
   time = millis();
   kart.reset();
}


/*----------------------------------------------------------------------
   get_box:  Given the current state, returns a number from 1 to 162
  designating the region of the state space encompassing the current state.
  Returns a value of -1 if a failure state is encountered.
----------------------------------------------------------------------*/

float one_degree = 0.0174532f ; /* 2pi/360 */
float three_degrees = 0.05233333f;
float six_degrees  = 0.1047192f;
float nine_degrees = 0.15707963f;
float fifteen_degrees = 0.2617993877f;
float twelve_degrees = 0.2094384f;
float thirtyfive_degrees = 0.6108652f;
float fifty_degrees = 0.87266f;

public String reverse_box_old(int box){
  int x,x_dot,theta,theta_dot;
  int rest = 0;
  theta_dot = box / (3*3*6);
  rest = box - (theta_dot*3*3*6);
  theta= rest / (3*3);
  rest = rest - (theta*3*3);
  
  x_dot = rest / (3 );
  rest = rest - (x_dot*3);
  
  x= rest ;
  theta = ((theta >= 3) ? theta-2 : theta-3);
  String ret = "," + str(x-1) + "," + str(x_dot-1) + "," + str(theta) + "," + str(theta_dot-1);
  return ret;
}

public int get_box_old(float x_, float x_dot_,float theta_,float theta_dot_)
{
  int box=0;

  if (fails(x_,x_dot_,theta_,theta_dot_) == 1)          {return(-1);} /* to signal failure */
  
  if(notX == 0){
    if (x_ < -0.5f)             box = 0;
    else if (x_ < 0.5f)              box = 1;
    else                         box = 2;
  }
  if(notX == 1){
    box = 1;
  }

  if (x_dot_ < -0.5f)            ;
  else if (x_dot_ < 0.5f)                box += 3;
  else                      box += 6;

  if (theta_ < -six_degrees)          ;
  else if (theta_ < -one_degree)        box += 9;
  else if (theta_ < 0)            box += 18;
  else if (theta_ < one_degree)          box += 27;
  else if (theta_ < six_degrees)        box += 36;
  else                   box += 45;

  if (theta_dot_ < -six_degrees)   ;
  else if (theta_dot_ < six_degrees)  box += 54;
  else                                 box += 108;

  return(box);
}





public int fails(float x, float x_dot, float theta, float theta_dot)
{
  if ( ( (notX == 0)&&(x < -failure_x || x > failure_x  || theta < -failure_theta || theta > failure_theta) ) || ( (notX == 1) && (theta < -failure_theta || theta > failure_theta)  ) ){
        println("errore: "+x+", "+ x_dot+", "+ theta*180/PI+", "+ theta_dot);    
    
    return 1;
  }
  else{
    return 0;  
  }
}

public int get_box_toomuch(float x_, float x_dot_, float theta_, float theta_dot_)
{
  int box=0;

  if (fails(x_,x_dot_,theta_,theta_dot_) == 1)          {println(x_,x_dot_,theta_,theta_dot_,failures);return(-1);} /* to signal failure */

  int side = canvas_x/2;
  
  if      (x_ < - side*3/4)           box = 0;
  else if (x_ < -side/2)              box = 1;
  else if (x_ < -side/4)              box = 2;
  else if (x_ < 0)                    box = 3;
  else if (x_ < +side/4)              box = 4;
  else if (x_ < +side/2)              box = 5;
  else if (x_ < +side*3/4)            box = 6;
  else                                box = 7;

  if      (x_dot_ < -20)              box += 0;
  else if (x_dot_ < -10)              box += 8;
  else if (x_dot_ < -5)               box += 16;
  else if (x_dot_ < -1)               box += 24;
  else if (x_dot_ < 0)                box += 32;
  else if (x_dot_ < 1)                box += 40;
  else if (x_dot_ < 5)                box += 48;
  else if (x_dot_ < 10)               box += 56;
  else                                box += 64;

  if      (theta_ < -fifteen_degrees)         ;
  else if (theta_ < -nine_degrees)    box += 64;
  else if (theta_ < -six_degrees)     box += 64*2;
  else if (theta_ < -three_degrees)   box += 64*3;
  else if (theta_ < -one_degree)      box += 64*4;
  else if (theta_ < 0)                box += 64*5;
  else if (theta_ < one_degree)       box += 64*6;
  else if (theta_ < three_degrees)    box += 64*7;
  else if (theta_ < six_degrees)      box += 64*8;
  else if (theta_ < nine_degrees)     box += 64*9;
  else if (theta_ < fifteen_degrees)  box += 64*10;
  else                                box += 64*11;

  if      (theta_dot_ < -fifty_degrees)         ;
  else if (theta_dot_ < -thirtyfive_degrees)   box += 768;
  else if (theta_dot_ < -fifteen_degrees)      box += 768*2;
  else if (theta_dot_ < -nine_degrees)         box += 768*3;
  else if (theta_dot_ < -three_degrees)        box += 768*4;
  else if (theta_dot_ < -one_degree)           box += 768*5;
  else if (theta_dot_ < 0)                     box += 768*6;
  else if (theta_dot_ < one_degree)            box += 768*7;
  else if (theta_dot_ < three_degrees)         box += 768*8;
  else if (theta_dot_ < nine_degrees)          box += 768*9;
  else if (theta_dot_ < fifteen_degrees)       box += 768*10;
  else if (theta_dot_ < thirtyfive_degrees)    box += 768*11;
  else if (theta_dot_ < fifty_degrees)         box += 768*12;
  else                                         box += 768*13;

  return(box);
}

public int get_box_new_semi(float x_, float x_dot_, float theta_, float theta_dot_)
{
  int box=0;

  if (fails(x_,x_dot_,theta_,theta_dot_) == 1)          {println(x_,x_dot_,theta_,theta_dot_,failures);return(-1);} /* to signal failure */

  int side = canvas_x/2;
  if (x_ > 0){
    x_dot_ *= -1;
    theta_ *= -1;
    theta_dot_ *= -1;
  }

  if      (-abs(x_) < -side*3/4)              box = 0;
  else if (-abs(x_) < -side/4)                    box = 1;
  else if (-abs(x_) < 0)              box = 2;

  if      (x_dot_ < -20)              box += 0;
  else if (x_dot_ < -6)              box += 3;
  else if (x_dot_ < -1)               box += 6;
  else if (x_dot_ < -0.5f)               box += 9;
  else if (x_dot_ < 0)                box += 12;
  else if (x_dot_ < 0.5f)                box += 15;
  else if (x_dot_ < 1)                box += 18;
  else if (x_dot_ < 6)               box += 21;
  else if (x_dot_ < 20)              box += 24;
  else                                box += 27;

  if      (theta_ < -fifteen_degrees)         ;
  else if (theta_ < -nine_degrees)    box += 30;
  else if (theta_ < -three_degrees)     box += 30*2;
  else if (theta_ < -one_degree)   box += 30*3;
  else if (theta_ < -one_degree/2)      box += 30*4;
  else if (theta_ < 0)                box += 30*5;
  else if (theta_ < one_degree/2)       box += 30*6;
  else if (theta_ < one_degree)    box += 30*7;
  else if (theta_ < three_degrees)      box += 30*8;
  else if (theta_ < nine_degrees)     box += 30*9;
  else if (theta_ < fifteen_degrees)  box += 30*10;
  else                                box += 30*11;

  if      (theta_dot_ < -fifty_degrees)         ;
  else if (theta_dot_ < -thirtyfive_degrees)   box += 360;
  else if (theta_dot_ < -fifteen_degrees)      box += 360*2;
  else if (theta_dot_ < -nine_degrees)         box += 360*3;
  else if (theta_dot_ < -three_degrees)        box += 360*4;
  else if (theta_dot_ < -one_degree)           box += 360*5;
  else if (theta_dot_ < 0)                     box += 360*6;
  else if (theta_dot_ < one_degree)            box += 360*7;
  else if (theta_dot_ < three_degrees)         box += 360*8;
  else if (theta_dot_ < nine_degrees)          box += 360*9;
  else if (theta_dot_ < fifteen_degrees)       box += 360*10;
  else if (theta_dot_ < thirtyfive_degrees)    box += 360*11;
  else if (theta_dot_ < fifty_degrees)         box += 360*12;
  else                                         box += 360*13;

  return(box);
}