int pause = 0;
int start_pause = 1;
int selector = 0;
int selector_file = 0;
int manual = 1;
long last_input = 0;
int randomStart = 0;
int INIZIALIZZARE = 0;
int notX = 0;
float valoreIniziale = 0.9f; 

float F=0;
float RealF=0;

int canvas_x = 1000;
int canvas_y = 500;

int ALPHA_INDEX = 0;
int BETA_INDEX = 1;
int GAMMA_INDEX = 2;
int XSTART_INDEX = 3;
int THETASTART_INDEX = 4;
int MASS_INDEX = 5;

String stringRecordxp, stringRecordxd, stringRecordtp,stringRecordtd;
String stringRecordrispostatheta, stringRecordrispostax;

Cart kart;

PID pid_theta, pid_x;

CARLA carla_theta_p;
CARLA carla_theta_d;
CARLA carla_x_p;
CARLA carla_x_d;
Evaluator evaluator_theta;
Evaluator evaluator_x;

SuperCARLA superCarla;


color background = color(255,255,255);

public void setup() {  
  scale(1,-1);
  frameRate(50);
  size(1000,500);
  smooth();
  ALPHBETAGAMMA[ALPHA_INDEX] = ALPH_init;
  ALPHBETAGAMMA[BETA_INDEX] = BETA_init;
  ALPHBETAGAMMA[GAMMA_INDEX] = GAMMA_init;
  kart = new Cart(new PVector(0,0), PI+PI/4);

  ALPHBETAGAMMA[XSTART_INDEX] = kart.p_cart_start.x;
  ALPHBETAGAMMA[THETASTART_INDEX] = kart.theta_start;
  ALPHBETAGAMMA[MASS_INDEX] = kart.m2;
  
  pid_theta = new PID(1,0,1); //da kp 15 funziona
  pid_x = new PID(1,0,1);
  
  evaluator_theta = new Evaluator(15);
  evaluator_x = new Evaluator(15);

  carla_theta_p = new CARLA(evaluator_theta, 600, 0, 1000);
  carla_theta_d = new CARLA(evaluator_theta, 200, 0, 900);
  carla_x_p = new CARLA(evaluator_x, 150, 0, 500);
  carla_x_d = new CARLA(evaluator_x, 300,0 , 500);
  superCarla = new SuperCARLA(evaluator_theta,evaluator_x, carla_theta_p,carla_theta_d,carla_x_p,carla_x_d, 30,150);
  
    kart.reset();
}

public void draw() {
  translate(canvas_x/2,canvas_y*2.8f/4);
  scale(1,-1);
  if(pause == 0 && start_pause == 0){
  
      background(background);
      stroke(220);
      line(-1000,0,1000,0);
      
      stroke(220);
      line(0,-1000,0,1000);
      
     
      
      kart.update();
      
      //kart.learn(); //reinforcement learning completo, qlearning
      
      superCarla.routine(kart.p_cart.x, kart.theta - PI);
      
      
      kart.control(); //controllo automatico, 2 pid
      
      
      kart.renderBB8();
      
      parameters();
  }
  kart.lastUpdate = millis();
  if(millis()> 300){
    start_pause = 0;
  }
  
}


public void keyPressed(){
    if (key==' '){ 
      kart.reset();

  }
   
   else if (key=='p'){
     pause = 1-pause;
   }
   else if (key=='a'){
     selector = 0;
   }
   else if (key=='b'){
     selector = 1;
   }
   else if (key=='g'){
     selector = 2;
   }
   else if (key=='x'){
     selector = 3;
   }
      else if (key=='t'){
     selector = 4;
   }
      else if (key=='m'){
     selector = 5;
   }
   else if (key=='c'){
       saveStrings("dataxp.dat",stringRecordxp.split("n"));
       saveStrings("dataxd.dat",stringRecordxd.split("n"));
       saveStrings("datatp.dat",stringRecordtp.split("n"));
       saveStrings("datatd.dat",stringRecordtd.split("n"));
   }
   else if(key == 'd'){
     manual = 1-manual;
   }
   else if(key == 'r'){
     randomStart = 1-randomStart;
   }
   else if(key == 'j'){
     superCarla.setIteration_steps(60000);
     saveStrings("datax.dat",stringRecordrispostatheta.split("n"));
     saveStrings("datat.dat",stringRecordrispostax.split("n"));
   }
   
   else if (key=='1'){
     selector_file = 1;
   }
   else if (key=='2'){
     selector_file = 2;
   }
   else if (key=='3'){
     selector_file = 3;
   }
   else if (key=='4'){
     selector_file = 4;
   }
   else if (key=='5'){
     selector_file = 5;
   }
   else if (key=='0'){
     selector_file = 0;
   }
   else if (key=='s'){
    if(selector_file != 0){
      println("saved to data" + selector_file + "("+N_BOXES+").dat" );
      String exp[] = new String[N_BOXES];
      for (int i = 0; i< N_BOXES; i++){
        
        exp[i] = join(str(q_val[i]), ":") + ";" +str(i) + ";" + reverse_box_old(i);
        
      }
      
      saveStrings("data"+selector_file+ "("+N_BOXES+").dat",exp);
      
      selector_file = 0;
    }
   }
   else if (key=='w'){
    if(selector_file != 0){
      println("saved to data" + selector_file + "("+N_BOXES+").dat" );
      String exp[] = new String[N_BOXES];
      for (int i = 0; i< N_BOXES; i++){
        float average = average3(q_val[i][0], q_val[i][1], q_val[i][2]);
        exp[i] = str(max3index(q_val[i][0], q_val[i][1], q_val[i][2])) + ", " + str(average) + ", " + str(var3(q_val[i][0], q_val[i][1], q_val[i][2], average)) + ", " + reverse_box_old(i) ;
        //exp[i] = join(str(q_val[i]), ":") + ";" +str(i) + ";" + reverse_box_old(i);
        
      }
      
      saveStrings("data"+selector_file+ "("+N_BOXES+")analisi.dat",exp);
      
      selector_file = 0;
    }
   }
   else if (key=='l'){
      if(selector_file != 0){
        println("loaded from " + selector_file + ".dat" );
        String lines[] = loadStrings("data" + selector_file + "(" + N_BOXES +").dat");
        String lin[];
        for(int i = 0; i < N_BOXES; i++){
          lin = split(lines[i],":");
          q_val[i] = PApplet.parseFloat(lin);
        }
           
        selector_file = 0;
    }
   }
   
   
   
   
   else if(keyCode == UP){
     if(selector <3) {
       ALPHBETAGAMMA[selector] += (1-ALPHBETAGAMMA[selector])/10;
     }
     else if (selector == XSTART_INDEX){
       
      ALPHBETAGAMMA[selector] += 1;
     }
     else if (selector == THETASTART_INDEX){
       ALPHBETAGAMMA[selector] += PI/180;
     }
     else if (selector == MASS_INDEX){
       ALPHBETAGAMMA[selector] += 0.1f;
     }
   }
   
   else if(keyCode == DOWN){
     if(selector <3) {
       ALPHBETAGAMMA[selector] -= (ALPHBETAGAMMA[selector])/10;
     }
     else if (selector == XSTART_INDEX){
       ALPHBETAGAMMA[selector] -= 1;
     }
     else if (selector == THETASTART_INDEX){
       ALPHBETAGAMMA[selector] -= PI/180;
     }
     else if (selector == MASS_INDEX){
       ALPHBETAGAMMA[selector] -= 0.1f;
     }
   }
   
   if(manual == 1){
     if(keyCode == RIGHT){
       input_action = 1;
       last_input = millis();
     }
     if(keyCode == LEFT){
       input_action = -1;
       last_input = millis();
     }
   }
}



void parameters(){
  pushMatrix();
  resetMatrix();
  stroke(244,0,0);
  text("#: " + failures,500,380);
  text("TIME: " + int((millis()-time)/1000.0),500,400);
  text("RECORD TIME: " + timeRecord/1000.0,10,30);
  text("THETA: " + ((kart.theta-PI)/PI*180),10,50);
  text("X: " + (kart.p_cart.x),10,70);
  text("X_dot: " + (kart.p_cart_dot.x),10,90);
  text("theta_dot: " + (kart.theta_dot/PI*180),10,110);
  text("X kp: " +superCarla.carla_x_p.parameter + " |mu:"+superCarla.carla_x_p.mu+" |var:"+superCarla.carla_x_p.var  ,200,130-100);
  text("X kd: " +superCarla.carla_x_d.parameter + " |mu:"+superCarla.carla_x_d.mu+" |var:"+superCarla.carla_x_d.var  ,200,150-100);
  text("Theta kp: " +superCarla.carla_theta_p.parameter + " |mu:"+superCarla.carla_theta_p.mu+" |var:"+superCarla.carla_theta_p.var  ,200,170-100);
  text("Theta kd: " +superCarla.carla_theta_d.parameter + " |mu:"+superCarla.carla_theta_d.mu+" |var:"+superCarla.carla_theta_d.var  ,200,190-100);
  text("X Jk: " +superCarla.carla_x_p.evaluator.j_actual + " |Jmin:"+superCarla.carla_x_p.evaluator.minimum()+" |Jmedio:"+superCarla.carla_x_p.evaluator.average()  ,200,210-100);
  text("Theta Jk: " +superCarla.carla_theta_p.evaluator.j_actual + " |Jmin:"+superCarla.carla_theta_p.evaluator.minimum()+" |Jmedio:"+superCarla.carla_theta_p.evaluator.average()  ,200,230-100);

  text("F: " + F,200,250-100);

  printinput();
  text(frameRate ,canvas_x - 100,10);

  popMatrix();

}

void printinput(){
  fill(0);
  if(selector == 0){
    fill(230,0,0);
  }
  text("ALPHA: " + ALPHBETAGAMMA[ALPHA_INDEX],10,canvas_y - 110);
  fill(0);
  if(selector == 1){
    fill(230,0,0);
  }
  text("BETA: " + ALPHBETAGAMMA[BETA_INDEX],10,canvas_y - 90);
  fill(0);
  if(selector == 2){
    fill(230,0,0);
  }
  text("GAMMMA: " + ALPHBETAGAMMA[GAMMA_INDEX],10,canvas_y - 70);
  fill(0);
  if(selector == 3){
    fill(230,0,0);
  }
  text("X start: " + ALPHBETAGAMMA[XSTART_INDEX],10,canvas_y - 50);
  fill(0);
  if(selector == 4){
    fill(230,0,0);
  }
  text("theta start: " + (int)((ALPHBETAGAMMA[THETASTART_INDEX]-PI)/PI*180),10,canvas_y - 30);
  fill(0);
  if(selector == 5){
    fill(230,0,0);
  }
  text("mass: " + abs(ALPHBETAGAMMA[MASS_INDEX]),10,canvas_y - 10);
  fill(0);

}