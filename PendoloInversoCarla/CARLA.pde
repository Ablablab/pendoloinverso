class SuperCARLA{
  public Evaluator evaluator_theta;
  public CARLA carla_theta_p;
  public CARLA carla_theta_d;
  public Evaluator evaluator_x;
  public CARLA carla_x_p;
  public CARLA carla_x_d;
  private int iteration_steps;
  private int iteration_steps_x;
  private int step = 0;
  private int begin = 1;
  
  public void setIteration_steps(int value){
    iteration_steps_x = value;
  }
  public void routine(float x, float theta){
    if ( begin == 1){
      step = 0;
      begin = 0;
      
      pid_theta.setP(carla_theta_p.new_parameter());
      pid_theta.setD(carla_theta_d.new_parameter());
      pid_x.setP(carla_x_p.new_parameter());
      pid_x.setD(carla_x_d.new_parameter());
      println("begin: iterazione nuova con kp=" + pid_theta.getP() + "kd=" + pid_theta.getD()+ "begin: iterazione nuova con posizione  kp=" + pid_x.getP() + "kd=" + pid_x.getD());

    }

    
    if(step >= iteration_steps_x || fallimento(x,theta) == 1){
      println("----------------------fine iterazione-----------------------------------------------");
      failures++;
      float b = evaluator_theta.evaluate(step);
      evaluator_theta.new_iteration();
     
      if(fallimento(x,theta) == 1){
        carla_theta_p.end_iteration(b,1);
        carla_theta_d.end_iteration(b,1);
      }
      else{
        carla_theta_p.end_iteration(b,0);
        carla_theta_d.end_iteration(b,0);
      }

      pid_theta.setP(carla_theta_p.new_parameter());
      pid_theta.setD(carla_theta_d.new_parameter());
     
     
     //position
      float b_x = evaluator_x.evaluate(step);
      evaluator_x.new_iteration();
      
      if(fallimento(x,theta)==1){
        carla_x_p.end_iteration(b_x,1);
        carla_x_d.end_iteration(b_x,1);
      }
      else{
        carla_x_p.end_iteration(b_x,0);
        carla_x_d.end_iteration(b_x,0);
      }

      pid_x.setP(carla_x_p.new_parameter());
      pid_x.setD(carla_x_d.new_parameter());
      
      stringRecordrispostatheta = "";
      stringRecordrispostax = "";
      stringRecordxp += ""+superCarla.carla_x_p.mu + " "+superCarla.carla_x_p.var+" "+superCarla.carla_x_p.parameter+"n";
      stringRecordxd += ""+superCarla.carla_x_d.mu + " "+superCarla.carla_x_d.var+" "+superCarla.carla_x_d.parameter+"n";
      stringRecordtp += ""+superCarla.carla_theta_p.mu + " "+superCarla.carla_theta_p.var+" "+superCarla.carla_theta_p.parameter+"n";
      stringRecordtd += ""+superCarla.carla_theta_d.mu + " "+superCarla.carla_theta_d.var+" "+superCarla.carla_theta_d.parameter+"n";
      println("errore: iterazione nuova con kp=" + pid_theta.getP() + "kd=" + pid_theta.getD()+ "errore: iterazione nuova con posizione  kp=" + pid_x.getP() + "kd=" + pid_x.getD());
      kart.reset();
      step = 0;
      return;  
    }
    
    stringRecordrispostax += millis() + " " +x+"n";
    stringRecordrispostatheta += millis() + " " +theta+"n";
    
     evaluator_theta.update(theta);
     evaluator_x.update(x*3);
     step += 1;
  } 
  
  SuperCARLA(Evaluator ev_theta,Evaluator ev_x, CARLA Carla_theta_p,CARLA Carla_theta_d,CARLA Carla_x_p,CARLA Carla_x_d, int steps, int steps_x){
    evaluator_theta = ev_theta;
    evaluator_x = ev_x;
    carla_theta_p = Carla_theta_p;
    carla_theta_d = Carla_theta_d;
    carla_x_p = Carla_x_p;
    carla_x_d = Carla_x_d;
    iteration_steps = steps;
    iteration_steps_x = steps_x;
  }
  
  public int fallimento(float x, float theta){
    if ( theta < -PI/2 || theta > PI/2 ){
        println("errore: "+x+","+theta*180/PI);    
    
        return 1;
    }
    else{
      return 0;  
    }
  }

}
class CARLA {
  public Evaluator evaluator;
  private float mu = 0.0f;
  private float var = -1;
  private float x_max = 0.0f;
  private float x_min = 0.0f;
  private float a;
  private float parameter;
  
  CARLA(Evaluator ev, float A, float X_min, float X_max){
    evaluator = ev;
    x_max = X_max;
    x_min = X_min;
    mu =A;
    a = A;
    var = 100000;

  }
  
  public void failure(){
    var = var*1;
  }
  
  public void end_iteration(float beta, int failure){
    //mu = mu -  a*beta  * (parameter - mu)*var;
    float mu_new = (1-failure)*((parameter)*beta + (1-beta)*mu) + failure * mu;
    //float mu_new = (parameter)*beta + (1-beta)*mu;
    //var = evaluator.variance();

    //var =   (abs(mu_new-mu)*40*beta + 0.7*var*(1-beta))*(1-failure) + failure * var * 1.7;
    var =   (abs(mu_new-parameter)*beta +var*beta*0.4 + 1.1*var*(1-beta))*(1-failure) + failure * var * 1.45;
    if (var > 30000){
      var = 30000;
    }
    if(var <60){
      var = 60;
    }
    mu = mu_new;
    //if (mu <x_min){
    //  mu = x_min;
    //}
    if(mu > x_max){
      mu = x_max;
    }

    println("il nuovo mu: "+mu+ ", nuova var = " + var + " grazie al parametro " + parameter);
  }
  
  public float new_parameter(){
    return (parameter = gaussianGenerator());
  }
  
  
  private float gaussianGenerator(){
    float par = -1;
    while(par <0 || par>x_max){
      par = sqrt(var) *randomGaussian() + mu;
    }
    return par;
  }

}

class Evaluator{
  private float errSum = 0;
  private int history_age=0;
  private History_buffer history;
  public float j_actual = 0;
  Evaluator(int History){
    history_age = History;
    history = new History_buffer(history_age);
    history.reset();
  }
  
  //during the iteration, at each step
  public void update(float error_now){
    //incremental sum
    


    errSum += (abs(error_now)+1)*(abs(error_now)+1);

  }
  
  public void abort(){
    errSum = 0;
  }
  
  //after a suitable time
  public float evaluate(float step){
    float Jk = errSum/step;
    j_actual = Jk;
    errSum = 0;
    
    history.insert(Jk);
    float Jmed = history.average();
    float Jmin = history.minimum();
    float beta;
    if (Jmed != Jmin){
      beta = min(1.0f, max(0.0f, (Jmed - Jk)/(Jmed - Jmin) ));
    }
    else{
      if (Jk > Jmed){
        beta = 0.0f;
      }
      else{
        beta = 0.3f;
      }
    }
    println("J attuale = "+Jk+ " Jmed = "+Jmed+" Jmin = " +Jmin + " e beta= " + beta);
    return beta;
  }
  
  public void new_iteration(){
    errSum = 0;
  }
  
  public float variance(){
    return history.variance();
  }
  
  public float past(){
    return history.counter();
  }
  
  public float average(){
    return history.average();
  }
  
  public float minimum(){
    return history.minimum();
  }
  
  
  class History_buffer{
    private float history[];
    private int dim = 0;
    private int last = 0;
    private float min = -1.0f;
    private float average = -1.0f;
    private float variance = 0.0f;
    private float initial_variance = 1000;
    private float counter = 1;
    
    
    History_buffer(int Dim){
      history = new float[Dim];
      dim = Dim;
      initial_variance = 1000;
    }
    
    public void reset(){
      for(int index = dim -1;index >= 0; index--){
        history[index] = 0.0f;
      }
      last = 0;
      min = -1.0f;
      average = -1.0f;
      variance = -1.0f;
    }

//MUST BE POSITIVE
    public void insert(float value){

      history[last] = value;
      if (min < 0){
        min = value;
      }
      min=value; //serve per azzerare min
      last += 1;
      if(last == dim){
        last = 0;
      }
      
      average = 0.0f;
      float sum = 0.0f;
      float sumQuad = 0.0f;
      int counter = 0;
      
      for(int index = dim -1;index >= 0; index--){
        if(history[index] >0){
          counter += 1;
          average += history[index];
          sum += history[index];
          sumQuad += history[index]*history[index];
          if(history[index] < min){
            min = history[index];
          }
        }
      }
      if(counter <=1){
        average = value;
        variance = initial_variance;
      }
      else{
        average = average /counter;
        float variance_new = (sumQuad + counter * min * min - 2.0 * min * sum) / (counter - 1);
        variance = variance_new;
      }
    }
    
    public float counter(){
      return counter;
    }
    
    public float variance(){
      return variance;
    }
  
    public float average(){
      if(average >= 0.0f){
        return average;
      }
      else   
        return 0.0f;
    }  

    public float minimum(){
      if(min>=0.0f){
        return min;
      }
      else 
        return 0.0f;
    }

}