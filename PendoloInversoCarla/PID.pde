class PID {

  private float kp, kd, ki;
  private long lastTime = 0;
  private float lastInput = 0;
  private float errSum = 0;
  PID(float p, float i, float d){
    kp = p;
    ki = i;
    kd = d;
  }
  
  public float compute(float input){
    if(lastTime == 0){
      lastTime = millis();
      errSum = 0;
      lastInput = input;
      return 0;
    }
    float temporal_step = (millis() - lastTime)/1000.0f;
    
    errSum += (input * temporal_step);
    float dErr = (input - lastInput) / temporal_step;
    if(temporal_step == 0){
      dErr = 0.0f;
    }
    
    if (errSum > 0.1f){
      errSum = 0.1f;
    }
    if(errSum < -0.1f){
      errSum = -0.1f;
    }
    /*Compute PID Output*/
    float Output = kp * input + ki * errSum + kd * dErr;

    /*Remember some variables for next time*/
    lastInput = input;
    lastTime = millis();
    
    return Output; 
  }
  
  public void reset(){
    lastTime = millis();
    lastInput = 0;
    errSum = 0;
  
  }
  
  public void setP(float par){
    kp = par;
  }
  
  public void setI(float par){
    ki = par;
  }
  
  public void setD(float par){
    kd = par;
  }
  
  public float getP(){
    return kp;
  }
  
  public float getI(){
    return ki;
  }
  
  public float getD(){
    return kd;
  }
}